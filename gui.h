#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui/backends/imgui_impl_glfw.h>
#include <imgui/backends/imgui_impl_opengl3.h>

struct GUI
{
    GLFWwindow *window;

    double mouseX, mouseY;
    bool show = true;

    GUI(GLFWwindow * const pWindow) noexcept;
    ~GUI()
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }
    void render() noexcept;
};

inline GUI::GUI(GLFWwindow * const pWindow) noexcept
    : window(pWindow)
{
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, +[](GLFWwindow * const w, int key, int, int action, int) noexcept
    {
        if(action == GLFW_RELEASE)
            return;

        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(w));
        if(key == GLFW_KEY_ESCAPE)
            gui.show = !gui.show;
    });
    glfwGetCursorPos(window, &mouseX, &mouseY);
    glfwSetCursorPosCallback(window, +[](GLFWwindow * const w, double const x, double const y) noexcept
    {
        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(w));
        /*
        if(gui.show)
            return;
        */

        gui.mouseX = x;
        gui.mouseY = y;
    });

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
}
inline void GUI::render() noexcept
{
    glfwSetInputMode(window, GLFW_CURSOR, show ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

    ImGuiIO &io = ImGui::GetIO();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if(show)
        ImGui::ShowDemoWindow(&show);

    {
        ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
        ImGui::SetNextWindowSize(ImVec2(160.f, 100.f));
        ImGui::Begin("Информация", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
        if(ImGui::Button("Quit"))
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        ImGui::Text("%.2f ms, %.1f FPS", 1000.0f / io.Framerate, io.Framerate);
        ImGui::Text("ESC to toggle GUI focus");
        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
