#version 330 core
out vec2 pos;
void main()
{
    pos = vec2[]
    (
        vec2(-1.f, -1.f),
        vec2(-1.f,  1.f),
        vec2( 1.f, -1.f),
        vec2( 1.f, -1.f),
        vec2(-1.f,  1.f),
        vec2( 1.f,  1.f)
    )[gl_VertexID];
    gl_Position = vec4(pos, 1.f, 1.f);
}
