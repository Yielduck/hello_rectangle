#version 330 core
in vec2 pos;
uniform float uTime;
uniform vec2 uMouse;
layout(location = 0) out vec4 outColor;
void main()
{
    outColor = vec4(uMouse, 0.5f + 0.5f * sin(uTime), 1.f);
}
